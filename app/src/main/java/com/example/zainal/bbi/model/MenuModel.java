package com.example.zainal.bbi.model;
/*
 * kelas ini di sebut dengan data model yaitu bentuk abstraksi dari
 * permodelan data
 */
public class MenuModel {
    //atribut data yang ada di menu utama aplikasi
    private int imageResource;
    private String title;
    //constructor. yaitu sebuah method atau fungsi yang namanya sama dengan nama kelasnya
    //dan tidak menggukana jenis tipe data baik 'void' maupun tipe data lainnya seperti
    //int, string , boolean dan lain - lain
    public MenuModel() {
    }

    public MenuModel(int imageResource, String title) {
        this.imageResource = imageResource;
        this.title = title;
    }

    //getter and setter. yaiu method atau fungsi yang digunakan untuk
    //mengambil nilai 'get' atau menyimpan nilai 'set'
    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
