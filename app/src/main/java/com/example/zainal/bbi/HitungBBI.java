package com.example.zainal.bbi;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;

import pl.pawelkleczkowski.customgauge.CustomGauge;

public class HitungBBI extends AppCompatActivity {
    private MaterialEditText tinggiBadan, beratBadan/*, umur*/;
    private TextView titleBeratBadanText, beratBadanText, imt;
    private Button hitungBbi;
    private CardView hasilPerhitunganCardView;
    private CustomGauge gaugeChart;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hitung_bbi);
        //beri nama di action bar
        getSupportActionBar().setTitle("Hitung BBI");
        //tampilkan home button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // hubungkan layout dengan kode java melalui id
        addLayoutFromXml();
        //hitung bbi
        hitungBbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try  {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {

                }
                //proses hitung bbi
                setHitungBbi();
                //kosongkan edit text ketika button di sentuh
                clearEditText();
            }
        });

    }
    //fungsi untuk menangani segala yang ada di action bar atau toolbar.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    //hapus text view ketika button di klik
    private void clearEditText(){
        tinggiBadan.setText("");
        beratBadan.setText("");
        //umur.setText("");
    }
    //hubungkan layout dengan java code
    private void addLayoutFromXml(){
        tinggiBadan = (MaterialEditText) findViewById(R.id.tinggi_badan);
        beratBadan = (MaterialEditText) findViewById(R.id.berat_badan);
        //umur = (MaterialEditText) findViewById(R.id.umur);

        titleBeratBadanText = (TextView)findViewById(R.id.title_hasil_hitung_bbi);
        beratBadanText = (TextView)findViewById(R.id.hasil_hitung_bbi);

        hitungBbi = (Button)findViewById(R.id.hitung_bbi);

        hasilPerhitunganCardView = (CardView)findViewById(R.id.hasil_perhitungan_card_view);

        imt = (TextView)findViewById(R.id.index_masa_tubuh);

        gaugeChart = (CustomGauge)findViewById(R.id.gauge_chart);
    }

    private void hitungBBI(){
        Double berat = Double.valueOf(beratBadan.getText().toString().trim());
        Double tinggi = Double.valueOf(tinggiBadan.getText().toString().trim());
        //cek index masa tubuh (IMT)
        if (beratBadanIdeal(berat, tinggi) <= 18.4){
            //berat badan kurang
            hasilPerhitunganCardView.setVisibility(View.VISIBLE); //tampilkan card view hasil perhitungan
            hasilPerhitunganCardView.setCardBackgroundColor(Color.parseColor("#FF5252"));

            //set gaugeChart dan tampilkan textView hasil perhitungan IMT
            setCircleGrafik((int) beratBadanIdeal(berat, tinggi));
            imt.setText(String.valueOf(beratBadanIdeal(berat, tinggi)));

            //berat badan gemuk
            titleBeratBadanText.setTextColor(Color.WHITE);
            beratBadanText.setTextColor(Color.WHITE);
            titleBeratBadanText.setText(getResources().getString(R.string.berat_badan_kurang_title));
            beratBadanText.setText(getResources().getString(R.string.berat_badan_kurang));
        }else if (beratBadanIdeal(berat, tinggi) >= 18.5 && beratBadanIdeal(berat, tinggi) <= 24.9){
            //berat badan ideal
            hasilPerhitunganCardView.setVisibility(View.VISIBLE); //tampilkan card view hasil perhitungan
            hasilPerhitunganCardView.setCardBackgroundColor(Color.parseColor("#FFCDD2"));

            //set gaugeChart dan tampilkan textView hasil perhitungan IMT
            setCircleGrafik((int) beratBadanIdeal(berat, tinggi));
            imt.setText(String.valueOf(beratBadanIdeal(berat, tinggi)));

            //set warna text
            titleBeratBadanText.setTextColor(Color.parseColor("#212121"));
            beratBadanText.setTextColor(Color.parseColor("#212121"));
            titleBeratBadanText.setText(getResources().getString(R.string.berat_badan_ideal_title));
            beratBadanText.setText(getResources().getString(R.string.berat_badan_ideal));
        }else if (beratBadanIdeal(berat, tinggi) >= 25.0 && beratBadanIdeal(berat, tinggi) <= 29.9){
            //berat badan lebih
            hasilPerhitunganCardView.setVisibility(View.VISIBLE); //tampilkan card view hasil perhitungan
            hasilPerhitunganCardView.setCardBackgroundColor(Color.parseColor("#FF5252"));

            //set gaugeChart dan tampilkan textView hasil perhitungan IMT
            setCircleGrafik((int) beratBadanIdeal(berat, tinggi));
            imt.setText(String.valueOf(beratBadanIdeal(berat, tinggi)));

            //set warna text
            titleBeratBadanText.setTextColor(Color.WHITE);
            beratBadanText.setTextColor(Color.WHITE);
            titleBeratBadanText.setText(getResources().getString(R.string.berat_badan_lebih_title));
            beratBadanText.setText(getResources().getString(R.string.berat_badan_lebih));
        }else if (beratBadanIdeal(berat, tinggi) >= 30.0 && beratBadanIdeal(berat, tinggi) <= 39.9){
            //berat badan gemuk
            hasilPerhitunganCardView.setVisibility(View.VISIBLE); //tampilkan card view hasil perhitungan
            hasilPerhitunganCardView.setCardBackgroundColor(Color.parseColor("#F44336"));

            //set gaugeChart dan tampilkan textView hasil perhitungan IMT
            setCircleGrafik((int) beratBadanIdeal(berat, tinggi));
            imt.setText(String.valueOf(beratBadanIdeal(berat, tinggi)));

            //set warna text
            titleBeratBadanText.setTextColor(Color.WHITE);
            beratBadanText.setTextColor(Color.WHITE);
            titleBeratBadanText.setText(getResources().getString(R.string.berat_badan_gemuk_title));
            beratBadanText.setText(getResources().getString(R.string.berat_badan_gemuk));
        }else if (beratBadanIdeal(berat, tinggi) >= 40.0){
            //berat badan sangat gemuk
            hasilPerhitunganCardView.setVisibility(View.VISIBLE); //tampilkan card view hasil perhitungan
            hasilPerhitunganCardView.setCardBackgroundColor(Color.parseColor("#D32F2F"));

            //set gaugeChart dan tampilkan textView hasil perhitungan IMT
            setCircleGrafik((int) beratBadanIdeal(berat, tinggi));
            imt.setText(String.valueOf(beratBadanIdeal(berat, tinggi)));

            //set warna text
            titleBeratBadanText.setTextColor(Color.WHITE);
            beratBadanText.setTextColor(Color.WHITE);
            titleBeratBadanText.setText(getResources().getString(R.string.berat_badan_sangat_gemuk_title));
            beratBadanText.setText(getResources().getString(R.string.berat_badan_sangat_gemuk));
        }

    }

    private void setHitungBbi(){
        if (tinggiBadan.getText().toString().trim().matches("") && beratBadan.getText().toString().trim().matches("")){
            Toast.makeText(this, "Form Tingg dan Berat Kosong", Toast.LENGTH_SHORT).show();
        }else  if (tinggiBadan.getText().toString().trim().matches("")){
            Toast.makeText(this, "Form Tingg Kosong", Toast.LENGTH_SHORT).show();
        }else  if (beratBadan.getText().toString().trim().matches("")){
            Toast.makeText(this, "Form Berat Kosong", Toast.LENGTH_SHORT).show();
        }else {
            hitungBBI();
        }
    }
    //rumus perhitungan index masa tubuh (IMT)
    private double beratBadanIdeal(double berat, double tinggi){
        double bbi = berat / Math.pow((tinggi/100),2);
        return bbi;
    }

    private void setCircleGrafik(final int bmiResult){
        gaugeChart.setEndValue(bmiResult);
        new Thread() {
            public void run() {
                int i;
                for (i = 0; i <= bmiResult; i++) {
                    try {
                        final int finalI = i;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                gaugeChart.setValue(200 + finalI * 6);
                                //imt.setText((int) bmiResultText);
                            }
                        });
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

    }

}
