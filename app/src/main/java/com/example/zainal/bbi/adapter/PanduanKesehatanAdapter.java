package com.example.zainal.bbi.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zainal.bbi.R;
import com.example.zainal.bbi.model.MenuModel;
import com.example.zainal.bbi.model.PanduanKesehatanModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PanduanKesehatanAdapter extends ArrayAdapter<PanduanKesehatanModel> {

    public PanduanKesehatanAdapter(Context context, ArrayList<PanduanKesehatanModel> menuItems) {
        super(context, 0, menuItems);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        //ambil posisi data dari model data di kelas 'PANDUAN KESEHATAN MODEL'
        PanduanKesehatanModel panduanKesehatanModel = getItem(position);
        //cek apakah view bernilai 'null' atau kosong ? jika kosong maka lakukan inflate layout. yaitu menghubungkan layout
        //dengan adapternya
        if (view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.panduan_kesehatan_item, parent, false);
        }
        //hubungkan widget di layout dengan adapter
        ImageView image = (ImageView)view.findViewById(R.id.panduan_kesehatan_image);
        TextView title = (TextView)view.findViewById(R.id.panduan_kesehatan_title);
        //load image ke widget imageview
        Picasso.get()
                .load(panduanKesehatanModel.getImageResource())
                .into(image);
        //load text ke widget textview
        title.setText(panduanKesehatanModel.getTitle());
        //return atau beri nilai balik berupa view
        return view;
    }
}
