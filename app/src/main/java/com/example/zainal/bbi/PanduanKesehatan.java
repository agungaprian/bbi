package com.example.zainal.bbi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.zainal.bbi.adapter.PanduanKesehatanAdapter;
import com.example.zainal.bbi.model.PanduanKesehatanModel;

import java.util.ArrayList;

public class PanduanKesehatan extends AppCompatActivity {
    private ListView listMenuItem;
    private ArrayList<PanduanKesehatanModel> panduanKesehatanModels;
    private PanduanKesehatanAdapter adapter;
    private PanduanKesehatanModel item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panduan_kesehatan);
        //beri nama di action bar
        getSupportActionBar().setTitle("Panduan Kesehatan");
        //tampilkan home button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // hubungkan layout dengan kode java melalui id
        addLayoutFromXml();
        listMenuItem.setAdapter(adapter);
        menuItemData();
        menuItemClick(listMenuItem);

    }
    //fungsi untuk menangani segala yang ada di action bar atau toolbar.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void addLayoutFromXml(){
        listMenuItem = (ListView) findViewById(R.id.list_item_kesehatan);
        panduanKesehatanModels = new ArrayList<>();
        adapter = new PanduanKesehatanAdapter(this, panduanKesehatanModels);
    }

    private void menuItemData (){
        item = new PanduanKesehatanModel(R.drawable.panduan_kesehatan_1, "7 menu diet , 7 hari tanpa nasi cepat menurunkan berat badan" );
        panduanKesehatanModels.add(item);

        item = new PanduanKesehatanModel(R.drawable.panduan_kesehatan_3, "10 jenis olahraga sehat" );
        panduanKesehatanModels.add(item);

        item = new PanduanKesehatanModel(R.drawable.panduan_kesehatan_2, "Panduan diet" );
        panduanKesehatanModels.add(item);
    }

    private void menuItemClick(ListView listView){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(PanduanKesehatan.this, ArtikelPanduanKesehatan.class);
                    intent.putExtra("artikel", "Menu_Diet.pdf");
                    startActivity(intent);
                }else if (position == 1){
                    Intent intent = new Intent(PanduanKesehatan.this, ArtikelPanduanKesehatan.class);
                    intent.putExtra("artikel", "Olahraga_Sehat.pdf");
                    startActivity(intent);
                }else if (position == 2){
                    Intent intent = new Intent(PanduanKesehatan.this, ArtikelPanduanKesehatan.class);
                    intent.putExtra("artikel", "Panduan_Diet.pdf");
                    startActivity(intent);
                }
            }
        });
    }

}
