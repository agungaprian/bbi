package com.example.zainal.bbi.quis;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.zainal.bbi.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuisDetail extends AppCompatActivity {
    private List<Question> questionList;

    private int score = 0;
    private int quid = 0;
    private int index = 0;
    private Question currentQuestion;

    private TextView question, answer;
    private RadioButton rda,rdb;
    private Button buttonNext , buttonCheck;

    private CardView answerCard;

    private String quisFilePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quis_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //beri nama di action bar
        getSupportActionBar().setTitle(getIntent().getStringExtra("title"));
        //tampilkan home button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //ambil data dari intent
        quisFilePath = getIntent().getStringExtra("pertanyaan");
        //ambil data dari csv di folder assets
        initData();
        //acak kuis
        Collections.shuffle(questionList);
        currentQuestion = questionList.get(quid);
        //hubungkan layout dengan java code
        addLayoutFromXml();
        //tampilkan kuis
        setQuestionView();

    }

    //fungsi untuk menangani segala yang ada di action bar atau toolbar.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private void addLayoutFromXml(){
        question = (TextView)findViewById(R.id.question);
        answer = (TextView)findViewById(R.id.answer);
        rda = (RadioButton)findViewById(R.id.radio0);
        rdb = (RadioButton)findViewById(R.id.radio1);
        buttonCheck = (Button)findViewById(R.id.button_check);
        buttonNext = (Button)findViewById(R.id.button_next);
        answerCard = (CardView) findViewById(R.id.answer_card);
    }
    //fungsi untuk menampilkan pertanyaan
    private void setQuestionView(){
        question.setText(currentQuestion.getQuestion());
        rda.setText(currentQuestion.getOptA());
        rdb.setText(currentQuestion.getOptB());

        quid++;
    }
    //cek jawaban button
    public void setButtonCheck(View view){
        RadioGroup grp = (RadioGroup)findViewById(R.id.radioGroup1);
        RadioButton answerOptions = (RadioButton)findViewById(grp.getCheckedRadioButtonId());
        //cek kondisi jawaban apakah salah atau benar. dan tampilkan hasilnya jika salah atau benar
        if(currentQuestion.getAnswer().equals(answerOptions.getText())){
            score++;
            Log.i("Answer " + index++," Jawaban Anda Benar");
            Log.d("Score","Your score: "+ score);
            answerCard.setVisibility(View.VISIBLE);
            answer.setText("Jawaban Anda Benar");
            answer.setTextColor(getResources().getColor(R.color.md_blue_grey_50));
            answerCard.setBackgroundColor(getResources().getColor(R.color.md_green_700));
        }else {
            Log.i("Answer " + index++," Jawaban Anda Salah");
            answerCard.setVisibility(View.VISIBLE);
            answer.setText("Jawaban Anda Salah");
            answer.setTextColor(getResources().getColor(R.color.md_blue_grey_50));
            answerCard.setBackgroundColor(getResources().getColor(R.color.md_red_500));
        }
        buttonCheck.setVisibility(View.GONE);
        buttonNext.setVisibility(View.VISIBLE);
    }
    //fungsi untuk melanjutkan pertanyaan ketika sudah di cek benar apa salah
    public void setButtonNext(View view){
        if(quid < 10){
            currentQuestion = questionList.get(quid);
            setQuestionView();

            buttonCheck.setVisibility(View.VISIBLE);
            buttonNext.setVisibility(View.GONE);
            answerCard.setVisibility(View.GONE);
        }else{
            ResultActivity();
        }
    }
    //fungsi untuk membaca data kuis dari folder asset
    private void initData() {
        questionList = new ArrayList<>();
        try {
            InputStreamReader is = new InputStreamReader(getAssets().open(quisFilePath));

            BufferedReader reader = new BufferedReader(is);
            reader.readLine();
            String line;
            String[] lines;

            while ((line = reader.readLine()) != null) {
                lines = line.split(",");
                Question question = new Question();
                question.setQuestion(lines[0]);
                question.setOptA(lines[1]);
                question.setOptB(lines[2]);
                question.setAnswer(lines[3]);

                questionList.add(question);
            }
        } catch (IOException e) {

        }
    }

    private void optionOnBackPress(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Apa anda yakin tidak ingin menyelsaikan kuis ?")
                .setPositiveButton("Yakin", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                }).setNegativeButton("Tidak, Lanjutkan", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        builder.create().show();
    }
    //fungsi untuk manampilkan score hasil dari kuis yang sudah dijawab dengan menampilkan alert dialog
    private void ResultActivity(){
        //Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //inflate layout ke dalam alert dialog
        View view = getLayoutInflater().inflate(R.layout.activity_result, null);

        builder.setView(view)
                .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        quid = 0;
                        score = 0;
                        buttonNext.setVisibility(View.GONE);
                        buttonCheck.setVisibility(View.VISIBLE);
                        answerCard.setVisibility(View.GONE);
                        Collections.shuffle(questionList);
                        setQuestionView();
                    }
                })
                .setNegativeButton("Keluar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });

        TextView scoreTxtView = (TextView) view.findViewById(R.id.score);
        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar1);
        ImageView img = (ImageView) view.findViewById(R.id.img1);

        ratingBar.setRating(score/2);
        scoreTxtView.setText(String.valueOf(score));

        if(score == 0 || score == 1){
            img.setImageResource(R.drawable.score_0);
        }else if(score == 2 || score == 3){
            img.setImageResource(R.drawable.score_1);
        }else if(score == 4 || score == 5){
            img.setImageResource(R.drawable.score_2);
        }else if(score == 6 || score == 7){
            img.setImageResource(R.drawable.score_3);
        }else if(score == 8 || score == 9){
            img.setImageResource(R.drawable.score_4);
        }else if(score == 10){
            img.setImageResource(R.drawable.score_5);
        }

        //tampilkan alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
