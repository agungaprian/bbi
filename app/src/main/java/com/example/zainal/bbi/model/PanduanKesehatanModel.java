package com.example.zainal.bbi.model;

public class PanduanKesehatanModel {
    //atribut data yang ada di menu utama aplikasi
    private int imageResource;
    private String title;
    //private String summary;
    //constructor. yaitu sebuah method atau fungsi yang namanya sama dengan nama kelasnya
    //dan tidak menggukana jenis tipe data baik 'void' maupun tipe data lainnya seperti
    //int, string , boolean dan lain - lain
    public PanduanKesehatanModel() {
    }

    public PanduanKesehatanModel(int imageResource, String title) {
        this.imageResource = imageResource;
        this.title = title;
        //this.summary = summary;
    }
    //getter and setter. yaiu method atau fungsi yang digunakan untuk
    //mengambil nilai 'get' atau menyimpan nilai 'set'
    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    /*
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }*/
}
