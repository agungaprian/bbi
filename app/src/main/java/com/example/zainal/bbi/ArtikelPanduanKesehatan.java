package com.example.zainal.bbi;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.source.DocumentSource;

import java.net.URI;

public class ArtikelPanduanKesehatan extends AppCompatActivity {

    private static final String TAG = ArtikelPanduanKesehatan.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artikel_panduan_kesehatan);
        //beri nama di action bar
        getSupportActionBar().setTitle("Panduan Kesehatan");
        //tampilkan home button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //ambil data pdf dari activity panduanKesehatan
        String data = getIntent().getStringExtra("artikel");
        //baca file pdf
        readPdf(data);
    }
    //fungsi untuk menangani segala yang ada di action bar atau toolbar.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish(); //destroy atau hancurkan activity ketika homeButton di klik
        }
        return super.onOptionsItemSelected(item);
    }
    //destroy atau hancurkan activity ketika tombol back pada ponsel di tekan
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    //fungsi untuk membaca file pdf
    private void readPdf(String filePath){
        PDFView pdfView = (PDFView)findViewById(R.id.pdf_view); //hubungkan layout dengan java code
        //mulai baca file pdf dari folder Assets
        pdfView.fromAsset(filePath)
                //tampilkan toast bila pembacaan pdf Eror
                .onError(new OnErrorListener() {
                    @Override
                    public void onError(Throwable t) {
                        Toast.makeText(ArtikelPanduanKesehatan.this, "terjadi eror", Toast.LENGTH_SHORT).show();
                        Log.d(TAG , "Error " + t.getLocalizedMessage());
                    }
                })
                //tampilkan pdf pada device
                .load();
    }
}
