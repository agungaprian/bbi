package com.example.zainal.bbi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class TentangKami extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tentang_kami);
        //beri nama di action bar
        getSupportActionBar().setTitle("Tentang Kami");
        //tampilkan home button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    //fungsi untuk menangani segala yang ada di action bar atau toolbar.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
