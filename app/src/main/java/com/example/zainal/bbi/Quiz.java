package com.example.zainal.bbi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;

import com.example.zainal.bbi.quis.QuisDetail;

public class Quiz extends AppCompatActivity {
    private CardView level1, level2, level3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        //beri nama di action bar
        getSupportActionBar().setTitle("Quiz");
        //tampilkan home button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        onItemSelected();
    }
    //fungsi untuk menangani segala yang ada di action bar atau toolbar.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void onItemSelected(){
        level1 = (CardView)findViewById(R.id.level_1);
        level2 = (CardView)findViewById(R.id.level_2);
        level3 = (CardView)findViewById(R.id.level_3);

        level1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Quiz.this, QuisDetail.class);
                intent.putExtra("pertanyaan", "level_1_questions.csv");
                intent.putExtra("title", "Level 1");
                startActivity(intent);
            }
        });

        level2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Quiz.this, QuisDetail.class);
                intent.putExtra("pertanyaan", "level_2_questions.csv");
                intent.putExtra("title", "Level 2");
                startActivity(intent);
            }
        });

        level3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Quiz.this, QuisDetail.class);
                intent.putExtra("pertanyaan", "level_3_questions.csv");
                intent.putExtra("title", "Level 3");
                startActivity(intent);
            }
        });

    }
}
