package com.example.zainal.bbi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.zainal.bbi.adapter.BBIAdapter;
import com.example.zainal.bbi.model.MenuModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private GridView gridMenuItem;
    private ImageView imageCover;
    private ArrayList<MenuModel> menuModels;
    private BBIAdapter adapter;
    private MenuModel item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //ambil view dari xml
        gridMenuItem = (GridView) findViewById(R.id.list_item_card);
        imageCover = (ImageView)findViewById(R.id.img_cover);
        //inisialisasi arraylist
        menuModels = new ArrayList<>();
        //inisialisasi adapter dan ambil konteks serta objeck dari MenuModels
        adapter = new BBIAdapter(this, menuModels);
        //set adapter ke gridview
        gridMenuItem.setAdapter(adapter);
        //fungsi untuk menghandle klik di gridview
        menuItemClick(gridMenuItem);
        //data untuk gridview
        menuItemData();
    }
    //input data untuk grid View
    private void menuItemData(){
        item = new MenuModel(R.drawable.bbi0, "Hitung BBI" );
        menuModels.add(item);

        item = new MenuModel(R.drawable.panduan_kesehatan, "Panduan Kesehatan" );
        menuModels.add(item);

        item = new MenuModel(R.drawable.quiz, "Quiz" );
        menuModels.add(item);

        item = new MenuModel(R.drawable.tentang_kami_image, "Tentang kami" );
        menuModels.add(item);
    }

    //fungsi untuk mengnangani perpindahan activity tiap grid yang di klik berdasarkan posisi.
    //posisi dimulai dari 0 yang letaknya ada di grid sebelah kiri atas itu
    //sebelah kiri atas itu adalah posisi pertama dengan position (nomor index) 0
    private void menuItemClick(GridView gridMenuItem){
        gridMenuItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == 0){
                    Intent intent = new Intent(getApplicationContext(), HitungBBI.class);
                    startActivity(intent);
                } else if (position == 1){
                    Intent intent = new Intent(getApplicationContext(), PanduanKesehatan.class);
                    startActivity(intent);
                } else if (position == 2){
                    Intent intent = new Intent(getApplicationContext(), Quiz.class);
                    startActivity(intent);
                } else if (position == 3){
                    Intent intent = new Intent(getApplicationContext(), TentangKami.class);
                    startActivity(intent);
                }
            }
        });
    }

}
