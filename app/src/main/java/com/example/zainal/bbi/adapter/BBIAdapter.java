package com.example.zainal.bbi.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zainal.bbi.R;
import com.example.zainal.bbi.model.MenuModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BBIAdapter extends ArrayAdapter<MenuModel> {

    public BBIAdapter(Context context, ArrayList<MenuModel> menuItems) {
        super(context, 0, menuItems);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        //ambil posisi data dari model data di kelas 'MENU MODEL'
        MenuModel menuModel = getItem(position);
        //cek apakah view bernilai 'null' atau kosong ? jika kosong maka lakukan inflate layout. yaitu menghubungkan layout
        //dengan adapternya
        if (view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.menu_layout, parent, false);
        }
        //hubungkan widget di layout dengan adapter
        ImageView image = (ImageView)view.findViewById(R.id.menu_image);
        TextView title = (TextView)view.findViewById(R.id.menu_text);
        //load image ke widget imageview
        Picasso.get()
                .load(menuModel.getImageResource())
                .into(image);
        //load text ke widget textview
        title.setText(menuModel.getTitle());
        //return atau beri nilai balik berupa view
        return view;
    }
}
